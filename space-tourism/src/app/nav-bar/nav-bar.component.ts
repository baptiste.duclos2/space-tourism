import { ImageLoader } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  title!: string;
  description!: string;
  imgUrl!: string;

  ngOnInit(){
    this.title = "logo";
    this.description = "This is our company logo";
    this.imgUrl = "assets/shared/logo.svg";
  }
}
